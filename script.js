document.getElementById('login-button').addEventListener('click', () => {
    const usernameChilds = document.getElementById('username-input').childNodes;
    const passwordChilds = document.getElementById('password-input').childNodes;
    const username = usernameChilds[1].value;
    const password = passwordChilds[1].value;

    fetch('http://localhost:5000', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    }).then()
})