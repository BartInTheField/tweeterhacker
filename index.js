const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = 5000;

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json({
    limit: '50mb'
}));

app.post('/', function (req, res) {
    console.log(req.body);
    res.status(200);
    res.end();
});

app.use('/', express.static(__dirname));

app.listen(port, function () {
    console.log(`Hacker listening on port ${port}`)
})